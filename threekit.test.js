const {sum, reverseString, toMinutesAndHours, customSort } = require('./threekit');

describe("Threekit Technical Review", () => {

    test('addition is performed correctly', () => {
        expect(sum(1, 2)).toBe(3);
    });

    test('params are validated to be numbers prior to addition', () => {
        expect(sum(1, 'duck')).toBe("ERROR");
    });

    test('strings are reversed', () => {
        expect(reverseString('millimeter')).toBe('retemillim');
    });

    test('convert seconds to readable N Hour(s) and N minute(s)', () => {
        expect(toMinutesAndHours(3600)).toBe("1 hour(s) and 0 minute(s)");
        expect(toMinutesAndHours(3602)).toBe("1 hour(s) and 0 minute(s)");
        expect(toMinutesAndHours(27020)).toBe("7 hour(s) and 30 minute(s)");
    });

    test('an array of random integers is sorted', () => {
        testArr = [54, 63, 14, 78, 2, 3];
        sortedArr = [2, 3, 14, 54, 63, 78];
        const result = customSort(testArr);
        expect(result).toStrictEqual(sortedArr); // correctly sorted array values
        expect(result).toBe(testArr); // same array reference
    });

});
