/**
 * Validate that both a and b are numbers and then return the sum of a and b
 */
function sum(a, b) {
    // complete this function to pass the test
}

function reverseString(str) {
    // complete this function to pass the test
}

function toMinutesAndHours(seconds) {
    // complete this function to pass the test
}

function customSort(randomArr) {
    // complete this function to pass the test
}

module.exports = {
    sum,
    reverseString,
    toMinutesAndHours,
    customSort
}
